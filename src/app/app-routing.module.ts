import { NgModule } from "@angular/core";
import { Route, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from '@services/core';

const routes: Route[] = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full'
  },
  {
    path: 'about',
    //component: AboutPageComponent
    loadChildren: './pages/about/about.module#AboutPageModule'
  },
  {
    path: 'account',
    //component: AccountPageComponent
    loadChildren: './pages/account/account.module#AccountPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'content/:type',
    //component: ContentPageComponent
    loadChildren: './pages/content/content.module#ContentPageModule'
  },
  {
    path: 'price',
    //component: PricePageComponent
    //loadChildren: './pages/price/price.module#PricePageModule'
    loadChildren: () => import('./pages/price/price.module').then(m => m.PricePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      anchorScrolling: 'enabled',
      scrollPositionRestoration: 'enabled',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
