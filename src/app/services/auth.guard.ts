import { Injectable } from "@angular/core";
import { CanActivate } from '@angular/router';
import { LoginService } from '@services/core';

@Injectable()

export class AuthGuard implements CanActivate {

  constructor(private loginService: LoginService) { }

  canActivate() {
    const isLogin = this.loginService.isLogin;
    if (isLogin) {
      return true;
    } else {
      this.loginService.showModal();
      return false;
    }
  }
}
