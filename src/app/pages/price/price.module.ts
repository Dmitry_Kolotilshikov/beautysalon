import { NgModule } from "@angular/core";
import { PricePageComponent } from './price.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PricePageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: PricePageComponent
    }])
  ]
})

export class PricePageModule { }
