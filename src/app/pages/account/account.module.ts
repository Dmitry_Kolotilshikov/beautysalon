import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { AccountPageComponent } from './account.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AccountPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: AccountPageComponent
    }])
  ]
})

export class AccountPageModule { }
