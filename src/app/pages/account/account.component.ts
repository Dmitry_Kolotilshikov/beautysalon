import { Component, OnInit } from "@angular/core";
import { LoginService } from '@services/core';

@Component({
  selector: 'app-account',
  templateUrl: 'account.component.html',
  styleUrls: ['account.component.scss']
})
export class AccountPageComponent implements OnInit {

  public email: string;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.getInfo();
  }

  private getInfo(): void {
    this.loginService.getUserInfo().then(result => this.email = result);
  }

  public logout(): void {
    this.loginService.logout();
  }
}
