import { NgModule } from "@angular/core";
import { AboutPageComponent } from './about.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AboutPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: AboutPageComponent
    }])
  ]
})

export class AboutPageModule {

}
