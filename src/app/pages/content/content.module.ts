import { NgModule } from "@angular/core";
import { ContentPageComponent } from './content.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ContentPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: ContentPageComponent
    }])
  ]
})

export class ContentPageModule { }
